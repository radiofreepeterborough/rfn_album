<?php

namespace Drupal\rfn_album\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Link;


/**
 * RFN Album event subscriber.
 */
class RfnAlbumSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Kernel request event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Response event.
   */
  public function onKernelRequest(GetResponseEvent $event) {

    $config = \Drupal::service('config.factory')->getEditable('rfn_album.settings');
   
    // Have we set streaming url base? don't show the message on the settings page itself.
    if(!$config->get('streaming_url_base') && \Drupal::routeMatch()->getRouteName() != 'rfn_album.settings') {
      $txt = t('Please click here to do that now');
      $link = Link::fromTextAndUrl($txt, \Drupal\Core\Url::fromRoute('rfn_album.settings'));
      $txtFull = t('For audio streaming to work, you must configure the streaming audio base url. %link', [ '%link' => $link->toString()]);
      $this->messenger->addError($txtFull);
    }

    // Have we set a parental advisory logo image?  
    if(!$config->get('parental_advisory_logo') && \Drupal::routeMatch()->getRouteName() != 'rfn_album.parental_advisory_settings') {
      $txt = t('Please click here to do that now');
      $link = Link::fromTextAndUrl($txt, \Drupal\Core\Url::fromRoute('rfn_album.parental_advisory_settings'));
      $txtFull = t('You need to set an image to use for albums flagged for potentially offensive content.  If your archive doesn\'t have any of those, just set it to any image.  %link', [ '%link' => $link->toString()]);
      $this->messenger->addError($txtFull);
    }
  }

  /**
   * Kernel response event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   Response event.
   */
  public function onKernelResponse(FilterResponseEvent $event) {
   // $this->messenger->addStatus(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => ['onKernelRequest'],
      KernelEvents::RESPONSE => ['onKernelResponse'],
    ];
  }

}
