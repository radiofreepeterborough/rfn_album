<?php

namespace Drupal\rfn_album;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;

/**
 * Twig extension.
 */
class RfnAlbumTwigExtension extends \Twig_Extension {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction(
      'rfn_build_streaming_uri', function ($stub = NULL) {
        $config = \Drupal::service('config.factory')->getEditable('rfn_album.settings');
        $base = $config->get('streaming_url_base');
        if (!$base) {
            \Drupal::messenger()->addMessage('You need to visit the config page at /admin/config/system/rfn-streaming-settings to configure streaming');
        }

        return $base . $stub;
      }),
      new \Twig_SimpleFunction('rfn_parental_advisory_logo', function ($size = NULL) {
        $config = \Drupal::service('config.factory')->getEditable('rfn_album.settings');
        $paLogo = $config->get('parental_advisory_logo');
        $alt = $this->t('Warning - potentially offensive explicit lyrics');
        if ($size == 'small' || !$size) {
          $file =File::load($paLogo);
          $path = file_create_url($file->getFileUri());
          return"<br/><img src='$path' alt='". $this->t('Parental Advisory') ."' style='max-width: 250px;' />";
      
            //return "TODO - show the parental advisory image using fid $paLogo";
         // return "<img src='$paLogo' alt='$alt' class='parental-advisory-small' style='max-width: 80px;'>";
          }
        }
      ),
    ];
  }

}
