<?php

namespace Drupal\rfn_album\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Configure RFN Album settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rfn_album_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['rfn_album.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'rfn_album/rfn_album';

    $form['streaming_url_base'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Streaming Url Base'),
      '#default_value' => $this->config('rfn_album.settings')->get('streaming_url_base'),
      '#description' => $this->t('This is the base uri you will stream audio from.  The final url passed to the audio player will consist of this url + the field_media_streaming_uri value from the track entity'),
    ];

    // Has the user set a fallback image?  If so, show it to them.
    $fallback_description = '';
    $fallback_fid = $this->config('rfn_album.settings')->get('fallback_image_fid');
    if ($fallback_fid) {

      $imgUrl = \Drupal::entityTypeManager()->getStorage('file')->load($fallback_fid)->createFileUrl();
      $fallback_description .= "<img src='$imgUrl' alt='Fallback Image' class='fallback-image'  />";
      $fallback_description .= '<p><strong>' . $this->t('This is the image that will be used when there is no other image set for an album or artist') . '</strong></p>';

    }
    else {
      $fallback_description .= $this->t('No fallback image is set yet.  Please upload an image (png or jpg).  This image will be used in meta data when no cover art or artist image is available');
    }

    $form['fallback_image'] = [
      '#type' => 'managed_file',
      '#title' => t('Fallback Image'),
      '#description' => $fallback_description,
      '#upload_location' => 'public://files/',
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg', 'png'],
      ],
      '#default_value' => [
        $this->config('rfn_album.settings')->get('fallback_image_fid'),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue('streaming_url_base')) {
      $form_state->setErrorByName('streaming_url_base', $this->t('You must set a value for Streaming Url Base in order to stream audio.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('rfn_album.settings')
      ->set('streaming_url_base', $form_state->getValue('streaming_url_base'))
      ->save();

    $fid = $form_state->getValue(['fallback_image', 0]);
    if (!empty($fid)) {
      $file = File::load($fid);
      $file->setPermanent();
      $file->save();
      $this->config('rfn_album.settings')
        ->set('fallback_image_fid', $fid)
        ->save();
    }

    parent::submitForm($form, $form_state);
  }

}
