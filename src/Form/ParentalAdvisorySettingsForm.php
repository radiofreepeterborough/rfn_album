<?php

namespace Drupal\rfn_album\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Configure RFN Album Parental Advisory settings for this site.
 */
class ParentalAdvisorySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rfn_album_parental_advisory_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['rfn_album.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    // Do we have a value for the parental advisory fid?
    $fid = $this->config('rfn_album.settings')
      ->get('parental_advisory_logo');

    $description = $this->t('The parental advisory logo to show on albums and tracks marked as Parental Advisory for potentially offensive content');
    if($fid && $fid > 0) {
      $file =File::load($fid);
      $path = file_create_url($file->getFileUri());
      $description .= "<br/><img src='$path' alt='". $this->t('Parental Advisory') ."' style='max-width: 250px;' />";
    }
    else {
      $description .= '<br/><strong>' . $this->t('You have not yet selected an image to use for the Parental Advisory logo') .'</strong>';
    }
    $form['parental_advisory_logo'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Image to use for the Parental Advisory Logo'),
      '#description' => $description,
      '#upload_location' => 'public://file',
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg jpeg png gif'],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $fid = $form_state->getValue(['parental_advisory_logo', 0]);
    if (!empty($fid)) {
      $file = File::load($fid);
      $file->setPermanent();
      $file->save();
     $this->config('rfn_album.settings')
      ->set('parental_advisory_logo', $fid)
      ->save();
    }
    parent::submitForm($form, $form_state);
  }

}
