// Set up handler to move to the next track when track is done
var audioElements = document.getElementsByTagName('audio');
for(var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
        audio.addEventListener('ended', playNext);
}

(function ($, Drupal, drupalSettings) {

    Drupal.behaviors.jumpToTrack = {
        attach:
        function () {

            $(document).ready(
                function (context, settings) {
                    // only run the track jump once
                    if (window.trackJumped) {
                        return;
                    }

                    window.trackJumped = 1;
                    const queryString = window.location.search;
                    const urlParams = new URLSearchParams(queryString);
                    var trackNumber = urlParams.get('track');

                    if(!trackNumber) {
                         return;
                    }

                    stopAllPlayers();
                    var audioElements = document.getElementsByTagName('audio');

                    // Normalize Track Number
                    if(!Number.isInteger(parseInt(trackNumber))) { return; }
                    if(trackNumber <= 0) {
                        trackNumber = 1;
                    }
                    if(trackNumber > audioElements.length) {
                        trackNumber = audioElements.length;
                    }

                    audioElements[trackNumber - 1].scrollIntoView(false);

                    // Set up skip to track modal 
                    var modalContent = drupalSettings.rfn_album.playTrackModalContent;
                    console.log(modalContent);
                    confirmationDialog = Drupal.dialog(modalContent, {
                        dialogClass: 'confirm-dialog',
                        resizable: false,
                        closeOnEscape: false,
                        width:600,
                        title: drupalSettings.rfn_album.playTrackModalTitle,
                        create: function () {
                         // $(this).parent().find('.ui-dialog-titlebar-close').remove();
                        },
                        beforeClose: false,
                        close: function (event) {
                            $(event.target).remove();
                            }
                        });
                        confirmationDialog.showModal();

                        $('#playButton').click( function() { 
                            $('.ui-dialog').remove();
                            const queryString = window.location.search;
                            const urlParams = new URLSearchParams(queryString);
                            var trackNumber = urlParams.get('track');

                            if(!trackNumber) {
                                return;
                            }

                            stopAllPlayers();
                            var audioElements = document.getElementsByTagName('audio');

                            // Normalize Track Number
                            if(!Number.isInteger(parseInt(trackNumber))) { return; }
                            if(trackNumber <= 0) {
                                trackNumber = 1;
                            }
                            if(trackNumber > audioElements.length) {
                                trackNumber = audioElements.length;
                            }

                            audioElements[trackNumber - 1].play();
                         });
                }
            );
        }
    }

})(jQuery, Drupal, drupalSettings);

function playNext(event) {

    var thisElement = event.target;
    var audioElements = document.getElementsByTagName('audio');

    // Where is this player in the list?
    for(var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
        if(audio == thisElement) {

            var next = i + 1;
            if(i >= audioElements.length - 1 ) {
                next = 0;
            }

            stopAllPlayers();
            audioElements[next].play();
            audioElements[next].scrollIntoView(false);
        }
    }
}

function stopAllOtherPlayers(event)
{

    var audioElements = document.getElementsByTagName('audio');
    var thisElement = event.target;

    for(var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
        if(audio != thisElement) { // If this element is not the one that was clicked, stop audio
            audio.pause();
        }
    }
}

function stopAllPlayers() {

    var audioElements = document.getElementsByTagName('audio');

    for (var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
        audio.pause();
    }
}

