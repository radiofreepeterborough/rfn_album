<?php

/**
 * @file
 * Tokens - Defines custom tokens for album entities.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function rfn_album_token_info() {

  $type = [
    'name' => t('RFN Album Tokens'),
    'description' => t('Tokens for album entities.'),
  ];

  $node['artist_for_album'] = [
    'name' => t('Artist for Album'),
    'description' => t('The Artist this album belongs to'),
  ];

  $node['image_uri_for_album_or_artist'] = [
    'name' => t('Image for Album or Artist'),
    'description' => t('Image uri for metatags to show cover art, artist image or fallback image'),
  ];

  return [
    'types' => ['rfn_album' => $type],
    'tokens' => ['rfn_album' => $node],
  ];
}

/**
 * Implements hook_tokens().
 */
function rfn_album_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {

  $replacements = [];

  if ($type == 'rfn_album' && !empty($data['node'])) {
    foreach ($tokens as $name => $original) {

      switch ($name) {
        case 'artist_for_album':
          $artist = $data['node']->get('field_artists')->first()->get('entity')->getTarget()->getValue();
          $replacements[$original] = $artist->getTitle();
          break;

        case 'image_uri_for_album_or_artist':
          $replacements[$original] = _rfn_album_fetch_image_uri_for_token($data);
          break;
      }
    }
  }
  return $replacements;
}

/**
 * Given a node, figure out which image to use, or fallback.
 */
function _rfn_album_fetch_image_uri_for_token($data) {

  $serverPrefix = 'http://';
  if (@$_SERVER['HTTPS']) {
    $serverPrefix = 'https://';
  }
  $serverPrefix .= $_SERVER['SERVER_NAME'];

  switch ($data['node']->getType()) {

    case 'album':
      // This is an album.  Send back the cover art.
      $fid = $data['node']->get('field_cover_art')->getValue()[0]['target_id'];
      $f = \Drupal::entityTypeManager()->getStorage('file')->load($fid);
      $url = $serverPrefix . $f->createFileUrl();

      // If this is a NOART cover, use the fallback image for meta data.
      if (preg_match("/\/NOART\.jpg$/", $url)) {
        $fallback_fid = \Drupal::configFactory()->getEditable('rfn_album.settings')->get('fallback_image_fid');
        if ($fallback_fid) {
          return $serverPrefix . \Drupal::entityTypeManager()->getStorage('file')->load($fallback_fid)->createFileUrl();
        }
      }

      return $url;

    case 'artist':
      $fid = $data['node']->get('field_images')->getValue()[0]['target_id'];
      $f = \Drupal::entityTypeManager()->getStorage('file')->load($fid);
      if ($f) {
        return $serverPrefix . $f->createFileUrl();
      }
      else {
        // There is no artist image set - use fallback.
        $fallback_fid = \Drupal::configFactory()->getEditable('rfn_album.settings')->get('fallback_image_fid');
        if ($fallback_fid) {

          return $serverPrefix . \Drupal::entityTypeManager()->getStorage('file')->load($fallback_fid)->createFileUrl();
        }
      }
      break;
  }
  // If we get this far, just use the fallback image.
  $fallback_fid = \Drupal::configFactory()->getEditable('rfn_album.settings')->get('fallback_image_fid');
  if ($fallback_fid) {

    return $serverPrefix . \Drupal::entityTypeManager()->getStorage('file')->load($fallback_fid)->createFileUrl();
  }
}
